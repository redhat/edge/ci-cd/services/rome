#!/usr/bin/env python3

import publisher


def main() -> None:
    """main function"""
    publisher.publish()


if __name__ == "__main__":
    main()
