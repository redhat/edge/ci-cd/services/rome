import json
import logging
import time

import pika

""" Configures and establishes a connection on the channel with the exchange
    Publish messages in loop, a delay of 2 sec is applied temorarily to avoid
    flooding the consumer queue"""


def publish():
    with open("./config/config.json") as ConfigFile:
        data = json.load(ConfigFile)

    connection = pika.BlockingConnection(pika.ConnectionParameters())

    channel = connection.channel()

    channel.exchange_declare(
        data["exchange"]["name"],
        data["exchange"]["exchange_type"],
        data["exchange"]["durable"],
    )

    """Loops over to send dummy configured messages (refer the JSON config).
    It picks routing keys randomly from the list of routing keys configured"""

    for index in range(int(data["app_config"]["message_iteration"])):
        try:
            for message in data["messages"]:
                channel.basic_publish(
                    data["exchange"]["name"],
                    message["routing_keys"],
                    properties=pika.BasicProperties(
                        headers=message["header"],
                        message_id=message["message_id"],
                        content_encoding=message["content_encoding"],
                        app_id=data["publisher"]["app_id"],
                        content_type="application/json",
                    ),
                    body=json.dumps(message["body"]),
                )
                logging.info(
                    f"[x] Iteration == {index + 1}, sent message == "
                    f'{message["body"]}: \n routing_key == '
                    f'{message["routing_keys"]} \n\n'
                )
                time.sleep(2.0)
        except ValueError:
            logging.error(
                f"There was an exception: {ValueError}, please restart the "
                "publisher"
            )
            connection.close()
