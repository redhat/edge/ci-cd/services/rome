import json

import pkg_manifest_schema


def test_serialize(json_file_data: str):
    test_obj = pkg_manifest_schema.PackageManifestSchema()
    manifest_data = json.loads(json_file_data)
    assert len(test_obj.common) == 0
    test_obj.deserialize(manifest_data)
    assert len(test_obj.common) == 1


def test_deserialize(json_file_data: str):
    test_obj = pkg_manifest_schema.PackageManifestSchema()
    test_obj.deserialize(json.loads(json_file_data))
    py_data = test_obj.serialize()
    file_data_str: str = json.dumps(py_data, separators=(",", ":"))
    assert json_file_data == file_data_str
