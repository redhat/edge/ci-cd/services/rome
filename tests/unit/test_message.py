import json
import logging
from unittest import mock

import requests
import requests_mock
import responses
from _pytest.logging import LogCaptureFixture

import message
from message import Message


def test_fetch_upstream_manifest() -> bool:
    with requests_mock.mock() as m:
        m.get(
            "https://gitlab.com/redhat/automotive/automotive-sig/-/raw/"
            "main/package_list/cs9-image-manifest.lock.json",
            text='["cs9", {"common":["qemu-img-6.2.0-10.el9", \
            "initscripts-service-10.11.2-1.el9"]}]',
        )
        assert message.get_upstream_manifest("cs9_manifest_url") == [
            "cs9",
            {
                "common": [
                    "qemu-img-6.2.0-10.el9",
                    "initscripts-service-10.11.2-1.el9",
                ]
            },
        ]


def test_fetch_upstream_manifest_with_exception(
    caplog: LogCaptureFixture,
) -> bool:
    caplog.set_level(logging.ERROR)
    with mock.patch("requests.get") as requests_get:
        requests_get.side_effect = requests.exceptions.RequestException
        value = message.get_upstream_manifest("cs9_manifest_url")
    assert value == "" and "RequestException" in caplog.text


@responses.activate
def test_submit_package(submit_package_vars: list, nvrs: list) -> bool:
    responses.add(
        responses.POST,
        url="http://dover:8080/submit/package_list",
        body="Package sent",
    )
    assert (
        message.submit_package(
            submit_package_vars[2][0],
            submit_package_vars[1],
            ",".join(nvrs),
            submit_package_vars[2],
            "cs9-image-manifest.lock.json",
        )
        == 200
    )


def test_submit_package_failed(
    caplog: LogCaptureFixture, submit_package_vars: list, nvrs: list
) -> bool:
    caplog.set_level(logging.ERROR)
    with mock.patch("requests.post") as requests_post:
        requests_post.side_effect = requests.exceptions.HTTPError
        value = message.submit_package(
            submit_package_vars[2][0],
            submit_package_vars[1],
            ",".join(nvrs),
            submit_package_vars[2],
            "cs9-image-manifest.lock.json",
        )
    assert value == 0 and "HTTPError" in caplog.text


def test_set_package_schema():
    test_obj = Message()
    assert len(test_obj.pkg_schema_obj.common) == 0
    test_obj.set_package_schema("cs9_manifest_url")
    assert len(test_obj.pkg_schema_obj.common) > 0


def test_parse_and_diff_success(json_file_data: str):
    test_obj = Message()
    test_obj.upstream_nvr = "qemu-img-6.2.0-10.el9"
    test_obj.build_id = 17482
    test_obj.pkg_schema_obj.deserialize(json.loads(json_file_data))
    result = test_obj.parse_and_diff()
    assert result == 1


def test_parse_and_diff_failure(json_file_data: str):
    test_obj = Message()
    test_obj.upstream_nvr = "abattis-cantarell-fonts-0.0.25-6.el8"
    test_obj.pkg_schema_obj.deserialize(json.loads(json_file_data))
    result = test_obj.parse_and_diff()
    assert result == 0


def test_call_diff_failure_with_partial_non_matching_nvr():
    nvr_list: list = ["libsmbclient-helper-4.14.5-2.el8"]
    test_obj = Message()
    test_obj.build_id = 17482
    test_obj.upstream_nvr = "qemu-img-6.2.0-10.el9"
    result, nvr_list = test_obj.call_diff(nvr_list)
    assert result == 0 and nvr_list[0] == "libsmbclient-helper-4.14.5-2.el8"


def test_call_diff_failure_with_same_nvr():
    nvr_list: list = ["qemu-img-6.2.0-10.el9"]
    test_obj = Message()
    test_obj.build_id = 17482
    test_obj.upstream_nvr = "qemu-img-6.2.0-10.el9"
    result, nvr_list = test_obj.call_diff(nvr_list)
    assert result == 0 and nvr_list[0] == "qemu-img-6.2.0-10.el9"


def test_call_diff_success():
    nvr_list: list = ["qemu-img-6.2.0-10.el9"]
    test_obj = Message()
    test_obj.build_id = 17482
    test_obj.upstream_nvr = "qemu-img-6.2.0-11.el9"
    result, nvr_list = test_obj.call_diff(nvr_list)
    assert result == 1 and nvr_list[0] == "qemu-img-6.2.0-11.el9"


def test_call_diff_with_dependent_nvrs():
    nvr_list: list = [
        "NetworkManager-1.36.0-2.el9",
        "NetworkManager-libnm-1.36.0-2.el9",
    ]
    test_obj = Message()
    test_obj.build_id = 17575
    test_obj.upstream_nvr = "NetworkManager-1.37.2-1.el9"
    result, nvr_list = test_obj.call_diff(nvr_list)
    assert result == 1 and nvr_list[1] == "NetworkManager-libnm-1.37.2-1.el9"


def test_call_diff_with_dependent_nvrs_on_x86_64():
    nvr_list: list = [
        "grub2-efi-x64-2.04-35.el9",
        "grub2-efi-x64-cdboot-2.04-35.el9",
        "grub2-pc-2.04-35.el9",
        "grub2-pc-modules-2.04-35.el9",
        "grub2-tools-efi-2.04-35.el9",
    ]
    test_obj = Message()
    test_obj.build_id = 17471
    test_obj.upstream_nvr = "grub2-efi-x64-2.06-25.el9"
    result, nvr_list = test_obj.call_diff(nvr_list)
    assert result == 1 and nvr_list[-1] == "grub2-tools-efi-2.06-25.el9"


@responses.activate
def test_send_message(
    submit_package_vars: list, json_file_data: str, message_body: str
):
    responses.add(
        responses.POST,
        url="http://dover:8080/submit/package_list",
        body="Package sent",
    )
    assert (
        message.submit_package(
            submit_package_vars[2][0],
            submit_package_vars[1],
            json_file_data,
            submit_package_vars[2],
            "cs9-image-manifest.lock.json",
        )
        == 200
    )
