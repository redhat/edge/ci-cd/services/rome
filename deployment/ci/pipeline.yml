---
- name: rome | Pipeline
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: tekton.dev/v1beta1
      kind: Pipeline
      metadata:
        name: rome
        namespace: "{{ ocp_ns }}"
      spec:
        workspaces:
          - name: shared-workspace
        params:
          - name: git-repo-url
            type: string
            description: url of the git repo for the code of deployment
            default: https://gitlab.com/redhat/edge/ci-cd/a-team/rome
          - name: git-revision
            type: string
            description: revision to be used from repo
            default: main
          - name: bucket-logs-url
            type: string
            description: the url prefix for logs bucket
            default: "https://{{ AWS_LOG_BUCKET }}.s3.{{ ATEAM_AWS_REGION }}.amazonaws.com"

        tasks:
          - name: fetch-repository
            taskRef:
              name: git-clone
              kind: ClusterTask
            workspaces:
              - name: output
                workspace: shared-workspace
            params:
              - name: url
                value: $(params.git-repo-url)
              - name: subdirectory
                value: ""
              - name: deleteExisting
                value: "true"
              - name: revision
                value: $(params.git-revision)
          - name: build-image
            taskRef:
              name: buildah
              kind: ClusterTask
            workspaces:
              - name: source
                workspace: shared-workspace
            params:
              - name: CONTEXT
                value: ./
              - name: IMAGE
                value: "image-registry.openshift-image-registry.svc:5000/{{ ocp_ns }}/rome:latest"
            runAfter:
              - fetch-repository
          - name: lint
            taskRef:
              name: linter-task
              kind: Task
            workspaces:
              - name: source
                workspace: shared-workspace
            runAfter:
              - build-image
          - name: unittest
            taskRef:
              name: unittest-task
              kind: Task
            workspaces:
              - name: source
                workspace: shared-workspace
            runAfter:
              - build-image
        finally:
          - name: gitlab-lint
            taskRef:
              name: gitlab-set-status
              kind: Task
            workspaces:
              - name: source
                workspace: shared-workspace
            params:
              - name: DESCRIPTION
                value: Lint passed
              - name: SHA
                value: $(params.git-revision)
              - name: STATE
                value: "$(tasks.lint.status)"
              - name: NAME
                value: linter
              - name: TARGET_URL
                value: "$(params.bucket-logs-url)/$(context.pipelineRun.name)_lint.log"

          - name: gitlab-unittests
            taskRef:
              name: gitlab-set-status
              kind: Task
            workspaces:
              - name: source
                workspace: shared-workspace
            params:
              - name: DESCRIPTION
                value: Tests passed
              - name: SHA
                value: $(params.git-revision)
              - name: STATE
                value: "$(tasks.unittest.status)"
              - name: NAME
                value: unittests
              - name: TARGET_URL
                value: "$(params.bucket-logs-url)/$(context.pipelineRun.name)_unittest.log"
