FROM registry.fedoraproject.org/fedora:34

RUN dnf -y install fedora-messaging \
    python3-devel \
    python3-pip \
    make \
    poetry \
    gcc \
    krb5-devel \
    &&  mkdir /home/rome \
    && pip3 install --upgrade pip==22.0.4


WORKDIR /home/rome

COPY . /home/rome

RUN pip3 install --no-cache-dir -r requirements.txt

# RUN poetry config virtualenvs.create false \
#     && poetry install \
#     && rm -rf ~/.cache

RUN dnf remove -y gcc python39-devel

ENTRYPOINT [ "./rome/main.py" ]
