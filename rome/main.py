#!/usr/bin/env python3
import logging
from os import path

from consumer import consume_message


def main() -> None:
    """main function"""
    log_config_path = path.join(
        path.dirname(path.abspath(__file__)), "configs/logging.conf"
    )
    logging.config.fileConfig(log_config_path)
    logging.getLogger("rome")
    logging.info("::Service started::")

    consume_message()


if __name__ == "__main__":
    main()
